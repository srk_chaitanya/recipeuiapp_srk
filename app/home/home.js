'use strict';

angular.module('myApp.home', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/home', {
    templateUrl: 'home/home.html',
    controller: 'homeCtrl'
  });
}])

.controller('homeCtrl', ['$scope','$http','$window','$location','ENV', function($scope, $http, $window, $location, ENV) {

    $http.get(ENV+'/api/users/'+sessionStorage.getItem('id')+'/recipes').success( function(data){
        $scope.smartTableData = data;
    }, function(err){
        toastMessage('Unable to fetch list of recipes.', true);
    })

    $scope.displayedData = angular.copy($scope.smartTableData);

    $scope.redirectToLogout = function(){
        sessionStorage.clear();  
        $location.path("login");
    }

    $scope.downloadDocument = function(recipeAttachmentId, isTransformedRecipe){
        $http({
            url: ENV+'/api/users/'+sessionStorage.getItem('id')+'/recipes/Download/' + recipeAttachmentId +'?isTransformedRecipe=' + isTransformedRecipe,
            type: 'GET',
            headers: {
                "Content-type": "application/json; charset=utf-8"
            },
            responseType: 'blob'
        })
        .success(function(response, status, headers){
            var data = response,
                contentDispositionHeader = headers('Content-Disposition'),
                url = $window.URL || $window.webkitURL;
            $scope.fileName = contentDispositionHeader && contentDispositionHeader.replace("attachment; filename*=utf-8''","");
            $scope.fileName = decodeURIComponent($scope.fileName);
            var blob = new Blob([data], {type: 'application/octet-stream'});
            
            $scope.fileUrl = url.createObjectURL(blob);
            setTimeout(function(){
                document.getElementById('download-row-content').click();
            }, 0);
            toastMessage('Document downloaded successfully.', true);
        })
        .error(function(){
            toastMessage('Unable to download document.', false);
        });
    }

    $scope.addDocument = function(){
        if($scope.myFile){
            uploadFileToUrl();
        }
    }

    function toastMessage(msg, type) {
        var x = document.getElementById("snackbar");
        $scope.serverMessage = msg;
        x.className = "show";
        x.style.background = type ? 'green' : 'red';
        setTimeout(function(){ 
            x.className = x.className.replace("show", ""); 
            // x.style.background = '';
        }, 3000);
    }

    function uploadFileToUrl(){
        var fd = new FormData();
        fd.append('file', $scope.myFile);
        $http.post(ENV+'/api/users/'+sessionStorage.getItem('id')+'/recipes/upload', fd, {
           transformRequest: angular.identity,
           headers: {'Content-Type': undefined}
        })
        .success(function(){
            toastMessage($scope.myFile.name + ' added successfully.', true);
        })
        .error(function(){
            toastMessage('Unable to add ' + $scope.myFile.name, false);
                      
        });
    }

    function downloadFile(){
        
    }

    $scope.$watch("myFile", function() {
        $scope.addDocument();
    });
}]);