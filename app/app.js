'use strict';

// Declare app level module which depends on views, and components
angular.module('myApp', [
  'ngRoute',
  'myApp.login',
  'myApp.signup',
  'myApp.home',
  'myApp.version',
  'myApp.fileReader',
  'smart-table',
  'config'
  
])

.config(['$locationProvider', '$routeProvider','$compileProvider', function($locationProvider, $routeProvider, $compileProvider) {
  $locationProvider.hashPrefix('!');
  $compileProvider.aHrefSanitizationWhitelist(/^\s*(|blob|):/);


  $routeProvider  
  .when("/login", {
    templateUrl : "login/login.html",
    controller: 'loginCtrl'
  })
  .when("/signup", {
    templateUrl : "signup/signup.html",
    controller: 'signUpCtrl'
  })
  .when("/home", {
    templateUrl : "home/home.html",
    controller: 'homeCtrl'
  })
  .when("/", {
    templateUrl : "login/login.html",
    controller: 'loginCtrl'
  });

  $routeProvider.otherwise({redirectTo: '/login'});
  
}
]);


